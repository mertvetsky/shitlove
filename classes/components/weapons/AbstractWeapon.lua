local AbstractWeapon = class:extend()

AbstractWeapon.delay = 0

function AbstractWeapon:use(position, x, y)
    local bullet = {}

    return bullet
end

function AbstractWeapon:isAvailable()
    return love.timer.getTime() > self.delay
end

function AbstractWeapon:updateDelay()
    self.delay = love.timer.getTime() + self.interval
end


return AbstractWeapon
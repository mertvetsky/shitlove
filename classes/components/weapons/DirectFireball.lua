-- пушка стреляющая прямо
local DirectFireball = classes.components.weapons.AbstractWeapon:extend()
DirectFireball.interval = 0.2

function DirectFireball:use(position, x, y)
    local direction = math.atan2(y - position.y, x - position.x)
    local shiftX, shiftY = 0, 0

    -- криво и костыльно, но пока сойдёт. тут должна использоваться позиция пушки, а не игрока
    while math.abs(shiftX) < 30 and math.abs(shiftY) < 30 do
        shiftX = shiftX + math.cos(direction)
        shiftY = shiftY + math.sin(direction)
    end

    local bullet = classes.entities.Bullet(position.x + shiftX, position.y + shiftY)
    bullet.continueMoving.getDeltas = function()
        return math.cos(direction), math.sin(direction)
    end

    self:updateDelay()

    return { bullet }
end

return DirectFireball
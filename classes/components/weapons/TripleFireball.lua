-- не было времени придумывать оригинальную механику
local TripleFireball = classes.components.weapons.AbstractWeapon:extend()
TripleFireball.interval = 0.2

function TripleFireball:use(position, x, y)
    local direction = math.atan2(y - position.y, x - position.x)
    local shiftX, shiftY = 0, 0

    -- криво и костыльно, но пока сойдёт. тут должна использоваться позиция пушки, а не игрока
    while math.abs(shiftX) < 30 and math.abs(shiftY) < 30 do
        shiftX = shiftX + math.cos(direction)
        shiftY = shiftY + math.sin(direction)
    end

    local bullet, bullets = {}, {}

    for _, delta in ipairs({ 0.9, 1, 1.1 }) do
        bullet = classes.entities.Bullet(position.x + shiftX, position.y + shiftY)
        bullet.continueMoving.getDeltas = function()
            -- неадекватные последствия дельты. Без учебника геометрии задумка обречена
            return math.cos(direction * delta), math.sin(direction * delta)
        end

        bullets[#bullets + 1] = bullet
    end
    self:updateDelay()

    return bullets
end

return TripleFireball
-- контролируемый объект, можем ему задать дельту перемещения
local Controlable = class:extend()

Controlable.dx = 0
Controlable.dy = 0

return Controlable
-- изображение, которое надо отрисовать. Потом надо вмазать анимацию и прочее
local Sprite = class:extend()

function Sprite:new(img, zindex)
    self.img = img
    self.zindex = zindex or 0
end

return Sprite
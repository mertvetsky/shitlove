-- положение в мире
local Position = class:extend()

function Position:new(x, y)
    self.x = x or 0
    self.y = y or 0
end

return Position
-- этот объект будет двигаться по указанной дельте
local Moving = class:extend()

function Moving:new()
    self:reset()
end

function Moving:reset()
    self.dx = 0
    self.dy = 0
end


return Moving
-- костыли для включения и выключения записи камеры
local gamera = vendor.gamera.gamera

function gamera:apply()
    love.graphics.setScissor(self:getWindow())

    love.graphics.push()
    local scale = self.scale
    love.graphics.scale(scale)
    love.graphics.translate((self.w2 + self.l) / scale, (self.h2 + self.t) / scale)
    love.graphics.rotate(-self.angle)
    love.graphics.translate(-self.x, -self.y)
end

function gamera:remove()
    love.graphics.pop()
    love.graphics.setScissor()
end

function gamera:draw(f)
    self:apply()
    f(self:getVisible())
    self:remove()
end

return gamera

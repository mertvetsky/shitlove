local Player = class:extend()

Player.sprite = classes.components.Sprite(assets.images.objects.player_2, 1)
Player.controllable = classes.components.Controllable()
Player.moving = classes.components.Moving()
Player.movingSpeed = classes.components.MovingSpeed(150)
Player.cameraPosition = classes.components.CameraPosition()
Player.hitbox = classes.components.Hitbox(30, 30)
Player.weapon = classes.components.weapons.TripleFireball()

function Player:new(x, y)
    self.position = classes.components.Position(x, y)
end

return Player
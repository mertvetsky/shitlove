local Bullet = class:extend()

Bullet.sprite = classes.components.Sprite(assets.images.objects.bullet_1, 1)
Bullet.movingSpeed = classes.components.MovingSpeed(220)
Bullet.hitbox = classes.components.Hitbox(10, 10)

function Bullet:new(x, y)
    self.moving = classes.components.Moving()
    self.position = classes.components.Position(x, y)
    self.continueMoving = classes.components.ContinueMoving()
end

function Bullet:onCollision(col)
    local other = col.other

    if col.other:is(classes.entities.Box) then
        world:remove(other)
    end

    if not col.other:is(Bullet) then world:remove(self) end
end

return Bullet

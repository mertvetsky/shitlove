local Box = class:extend()

Box.sprite = classes.components.Sprite(assets.images.objects.box_1, 1)
Box.hitbox = classes.components.Hitbox(20, 20)

function Box:new(x, y)
    self.position = classes.components.Position(x, y)
end



return Box
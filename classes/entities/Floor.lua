local Floor = class:extend()

Floor.sprite = classes.components.Sprite(assets.images.objects.floor_2, 0)

function Floor:new(x, y)
    self.position = classes.components.Position(x, y)
end

return Floor
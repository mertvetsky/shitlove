local Wall = class:extend()

Wall.sprite = classes.components.Sprite(assets.images.objects.wall_1, 0)
Wall.hitbox = classes.components.Hitbox(20, 20)

function Wall:new(x, y)
    self.position = classes.components.Position(x, y)
end

return Wall
-- рисование спрайтика
local SpriteSystem = tiny.processingSystem(class:extend())

function SpriteSystem:new(camera, zindex)
    self.camera = camera
    self.zindex = zindex or 0
    self.filter = tiny.requireAll("sprite", "position")
end

function SpriteSystem:preProcess(dt)
    self.camera:apply()
end

function SpriteSystem:postProcess(dt)
    self.camera:remove()
    love.graphics.setColor(1, 1, 1, 1)
end

function SpriteSystem:process(e, dt)
    if e.sprite.zindex ~= self.zindex then return end


    love.graphics.draw(e.sprite.img, e.position.x, e.position.y)
end

return SpriteSystem
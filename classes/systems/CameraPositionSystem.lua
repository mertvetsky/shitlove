-- следование камеры за объектом. Художественно скопировано и содержит магический код
local CameraPositionSystem = tiny.processingSystem(class:extend())

CameraPositionSystem.filter = tiny.requireAll("cameraPosition", "position")

function CameraPositionSystem:new(camera)
    self.camera = camera
end

local function round(x)
    return math.floor(x * 32 + 16) / 32
end

function CameraPositionSystem:process(e, dt)
    local xo, yo = e.cameraPosition.xoffset, e.cameraPosition.yoffset
    local x, y = e.position.x + xo, e.position.y + yo
    local xp, yp = self.camera:getPosition()
    local lerp = 0.02 -- эта какая-то магическая математика, отвечает за плавность перехода
    self.camera:setPosition(round(xp + (x - xp) * lerp), round(yp + (y - yp) * lerp))
end

function CameraPositionSystem:onAdd(e)
    local xo, yo = e.cameraPosition.xoffset, e.cameraPosition.yoffset
    local x, y = e.position.x + xo, e.position.y + yo
    self.camera:setPosition(round(x), round(y))
end

return CameraPositionSystem
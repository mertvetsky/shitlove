-- движение тех, кто должен двигаться в этом фрейме
local MovingSystem = tiny.processingSystem(class:extend())

function MovingSystem:new(bumpWorld)
    self.bumpWorld = bumpWorld
end

MovingSystem.filter = tiny.requireAll("position", "hitbox", "moving")

function MovingSystem:process(e, dt)
    if e.moving then
        local x = e.position.x + e.moving.dx
        local y = e.position.y + e.moving.dy
        local cols, len = {}, 0
        e.position.x, e.position.y, cols, len = self.bumpWorld:move(e, x, y)

        for i = 1, len do
            if e.onCollision then
                e:onCollision(cols[i])
            end
        end
        e.moving:reset()
    end
end

return MovingSystem
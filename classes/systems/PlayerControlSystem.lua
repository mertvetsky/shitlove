-- управление персонажем
local PlayerControlSystem = tiny.processingSystem(class:extend())

function PlayerControlSystem:new(camera)
    self.camera = camera
    self.filter = tiny.requireAll("controllable")
end

function PlayerControlSystem:process(p, dt)
    local lks = love.keyboard.isScancodeDown

    -- moving
    if p.moving and p.movingSpeed then
        local n, s, e, w = lks('up', 'w'), lks('down', 's'), lks('right', 'd'), lks('left', 'a')

        if n and not s then
            p.moving.dy = dt * p.movingSpeed.value * -1
        elseif s and not n then
            p.moving.dy = dt * p.movingSpeed.value
        end

        if w and not e then
            p.moving.dx = dt * p.movingSpeed.value * -1
        elseif e and not w then
            p.moving.dx = dt * p.movingSpeed.value
        end
    end

    -- use weapon
    if p.weapon and p.position then
        if love.mouse.isDown(1) and p.weapon:isAvailable() then
            local x, y = self.camera:toWorld(love.mouse.getX(), love.mouse.getY())

            for _, bullet in ipairs(p.weapon:use(p.position, x, y)) do
                world:add(bullet)
            end
        end
    end

    -- just debug
    if p.position then
        love.graphics.print("x: " .. p.position.x .. ", y: " .. p.position.y, 500, 0)
    end
end

return PlayerControlSystem

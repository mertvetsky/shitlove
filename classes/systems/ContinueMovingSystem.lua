-- обработка самостоятельного движения объектов
local ContinueMovingSystem = tiny.processingSystem(class:extend())

ContinueMovingSystem.filter = tiny.requireAll("continueMoving", "moving", "movingSpeed")

function ContinueMovingSystem:process(e, dt)
    local dx, dy = e.continueMoving:getDeltas()

    e.moving.dx = dx * dt * e.movingSpeed.value
    e.moving.dy = dy * dt * e.movingSpeed.value
end

return ContinueMovingSystem

-- регистрация хитбокса в физике
local HitboxSystem = tiny.processingSystem(class:extend())

function HitboxSystem:new(bumpWorld)
    self.bumpWorld = bumpWorld
end

HitboxSystem.filter = tiny.requireAll("position", "hitbox")

function HitboxSystem:onAdd(e)
    self.bumpWorld:add(e, e.position.x, e.position.y, e.hitbox.w, e.hitbox.h)
end

function HitboxSystem:onRemove(e)
    self.bumpWorld:remove(e)
end

return HitboxSystem
globalConfig = require 'globalConfig'
local cargo = require 'vendor/cargo/cargo'

-- assets
classes = cargo.init('classes')
vendor = cargo.init('vendor')
assets = cargo.init('assets')

-- alias
tiny = vendor.tiny.tiny
class = vendor.classic.classic

function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")
    math.randomseed(os.time() * 3.456)
    local bumpWorld = vendor.bump.bump.newWorld()
    local camera = classes.helpers.camera.new(0, 0, 15000, 15000)
    camera:setScale(1.5)

    _G.world = tiny.world(classes.systems.HitboxSystem(bumpWorld),
        classes.systems.SpriteSystem(camera, 0),
        classes.systems.SpriteSystem(camera, 1),
        classes.systems.MovingSystem(bumpWorld),
        classes.systems.PlayerControlSystem(camera),
        classes.systems.ContinueMovingSystem(),
        classes.systems.CameraPositionSystem(camera))


    -- временные костыли, должно переехать в инициализацию уровня
    addPlayer()
    addFloor()
    addWalls()
    addBoxes()
end

function love.update(dt)
end

function love.draw()
    world:update(love.timer.getDelta())
end

function addPlayer()
    world:add(classes.entities.Player(100, 100))
end

function addFloor()
    for i = 0, 55 do
        for j = 0, 55 do
            world:add(classes.entities.Floor(i * 20, j * 20))
        end
    end
end

function addWalls()
    for i = 0, 55 do
        for j = 0, math.random(0, 3) do
            world:add(classes.entities.Wall(i * 20, j * 20))
        end
    end

    for j = 0, 55 do
        for i = 0, math.random(0, 3) do
            world:add(classes.entities.Wall(i * 20, j * 20))
        end
    end
end


function addBoxes()
    for i = 6, 55 do
        for j = 6, 55 do
             if math.random(0, 20) == 0 then  world:add(classes.entities.Box(i * 20, j * 20)) end
        end
    end
end


function dump(...)
    print(vendor.inspect.inspect(...))
end